# coding: utf-8
from django.shortcuts import redirect
from django.core.urlresolvers import reverse


def index(request):
    if not request.user.is_authenticated():
        return redirect(reverse('accounts.views.login'))
    else:
        pass
        # return redirect(reverse('containers.views.index'))
